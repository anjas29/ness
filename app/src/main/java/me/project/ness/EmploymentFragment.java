package me.project.ness;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import me.project.ness.adapters.ListEmploymentAdapter;
import me.project.ness.adapters.ListNewsAdapter;
import me.project.ness.objects.News;


/**
 * A simple {@link Fragment} subclass.
 */
public class EmploymentFragment extends Fragment {
    private RecyclerView recyclerView;
    public static ArrayList<News> itemList;
    private LinearLayoutManager layoutManager;
    private ListEmploymentAdapter adapter;
    private ProgressBar progressBar;

    public EmploymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employment, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.content);

        itemList = new ArrayList<>();

        getData();

        layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new ListEmploymentAdapter(getContext(), itemList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    public void getData(){
        itemList.add(new News("PT Prospect Group", "20-10-2017", "https://siva.jsstatic.com/id/62019/images/logo/62019_logo_0_286134.jpg", "author", "Jakarta Utara (Jakarta Raya) - PLUIT"));
        itemList.add(new News("PT Mitra Pinasthika Mustika Auto", "20-10-2017", "https://siva.jsstatic.com/id/44937/images/logo/44937_logo_0_725165.jpg", "author", "Jakarta Barat (Jakarta Raya) - Cengkareng"));
        itemList.add(new News("PT Aprisma Wirecard", "20-10-2017", "https://siva.jsstatic.com/id/3265/images/logo/3265_logo_0_7093878.jpg", "author", "Jakarta Raya - jakarta barat"));
        itemList.add(new News("Magix Metro Media", "20-10-2017", "https://www.jobstreet.co.id/id/job-search/jobs-at-magix-metro-media/", "author", "Jakarta Selatan (Jakarta Raya) - Kuningan"));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem searchItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return true;
            }
        });
    }
}
