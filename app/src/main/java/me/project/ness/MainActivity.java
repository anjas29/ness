package me.project.ness;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Fragment currentFragment, dashboardFragment, employmentFragment, notificationFragment, acountFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dashboardFragment = new DashboardFragment();
        employmentFragment = new EmploymentFragment();
        notificationFragment = new NotificationFragment();
        acountFragment = new AccountFragment();

        if(savedInstanceState != null){
            currentFragment = getSupportFragmentManager().getFragment(savedInstanceState, "lastFragment");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content, currentFragment).commit();
        }else{
            currentFragment = dashboardFragment;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content, currentFragment).commit();
        }
        

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    currentFragment = dashboardFragment;
                    fragmentManager.beginTransaction().replace(R.id.content, currentFragment).commit();
                    return true;
                case R.id.navigation_account:
                    currentFragment = acountFragment;
                    fragmentManager.beginTransaction().replace(R.id.content, currentFragment).commit();
                    return true;
                case R.id.navigation_notifications:
                    currentFragment = notificationFragment;
                    fragmentManager.beginTransaction().replace(R.id.content, currentFragment).commit();
                    return true;
                case R.id.navigation_employment:
                    currentFragment = employmentFragment;
                    fragmentManager.beginTransaction().replace(R.id.content, currentFragment).commit();
                    return true;
            }
            return false;
        }

    };

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "lastFragment", currentFragment);
    }

}
