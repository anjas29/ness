package me.project.ness;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;

import me.project.ness.adapters.ListNewsAdapter;
import me.project.ness.objects.News;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    private RecyclerView recyclerView;
    public static ArrayList<News> itemList;
    private LinearLayoutManager layoutManager;
    private ProgressBar progressBar;

    private SliderLayout sliderLayout;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.content);
        sliderLayout = (SliderLayout) view.findViewById(R.id.slider);

        itemList = new ArrayList<>();

        getData();
        setBanner();

        layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        ListNewsAdapter adapter = new ListNewsAdapter(getContext(), itemList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    public void getData(){
        itemList.add(new News("Keadaan dan Info Tenaga Kerja Indonesia", "20-10-2017", "https://www.edunews.id/wp-content/uploads/2017/05/pabrik-mobil_20151214_091350.jpg", "author", "Data dan Satistik Tenaga Kerja di Indonesia Oktober 2017"));
        itemList.add(new News("Pengangguran di Indonesia", "20-10-2017", "https://imardianah.files.wordpress.com/2013/02/smk.jpg", "author", "pembangunan ekonomi mampu menambahkan banyak pekerjaan baru di Indonesia, yang dengan demikian mampu mengurangi angka pengangguran nasional."));
        itemList.add(new News("Tingkat Partisipasi Angkatan Kerja", "19-10-2017", "https://lh3.googleusercontent.com/-lIXdTzhG3I4/UBdKeQRmDtI/AAAAAAAABoo/4CH9JqD_qbg/s414/Auto-ind.jpg", "author", "Tingkat Partisipasi Angkatan Kerja (TPAK) adalah Persentase jumlah angkatan kerja terhadap penduduk usia kerja"));
    }

    public void setBanner(){
        TextSliderView textSliderView = new TextSliderView(getContext());

        textSliderView
                .description("Informasi Tenga Kerja")
                .image("http://nusakini.com/upload/image/media/N8FPrDuBEo_1502874353.jpg")
                .setScaleType(BaseSliderView.ScaleType.Fit);

        sliderLayout.addSlider(textSliderView);

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        sliderLayout.setPresetTransformer("ZoomOut");
    }

}
