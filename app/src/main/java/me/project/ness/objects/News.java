package me.project.ness.objects;

/**
 * Created by anjas on 28/10/17.
 */

public class News {
    String name;
    String created_at;
    String image;
    String author;
    String description;

    public News(String name, String created_at, String image, String author, String description) {
        this.name = name;
        this.created_at = created_at;
        this.image = image;
        this.author = author;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
