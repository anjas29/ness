package me.project.ness;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.daimajia.slider.library.SliderLayout;

import java.util.ArrayList;

import me.project.ness.adapters.ListNewsAdapter;
import me.project.ness.objects.News;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {
    private RecyclerView recyclerView;
    public static ArrayList<News> itemList;
    private LinearLayoutManager layoutManager;
    private ProgressBar progressBar;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.content);

        itemList = new ArrayList<>();

        getData();

        layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        ListNewsAdapter adapter = new ListNewsAdapter(getContext(), itemList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    public void getData(){
        itemList.add(new News("Pelatihan Industri Kreatif (Membatik)", "20-10-2017", "http://akademitrainer.com/wp-content/uploads/2016/11/111-300x199.jpg", "author", "Senin , 13 November 2017"));
        itemList.add(new News("Pelatihan Bordir", "20-10-2017", "http://www.segmennews.com/wp-content/uploads/2014/06/ilus.jpg", "author", "Kamis , 16 November 2017"));
        itemList.add(new News("Kursus Administrasi Perkantoran", "20-10-2017", "http://www.afafkomputer.com/wp-content/uploads/2017/01/kursus-administrasi-perkantoran.png", "author", "Kamis , 16 November 2017"));
        itemList.add(new News("Pelatihan Desain Grafis", "20-10-2017", "http://www.amikbnc.ac.id/system/application/views/web/berita/pelatihan-degraf.jpg", "author", "Sabtu , 18 November 2017"));
    }

}
