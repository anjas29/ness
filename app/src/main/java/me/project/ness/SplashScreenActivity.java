package me.project.ness;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sharedPreferences = getSharedPreferences("sData", Context.MODE_PRIVATE);

        //startService(new Intent(this, PusherService.class));

        Thread timerThread = new Thread(){
            public void run(){
                try{

//                    startService(new Intent(getApplicationContext(), EventPusherService.class));

                    sleep(2000);
                    if(sharedPreferences.getBoolean("login", false)){
                        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else if (sharedPreferences.getBoolean("first", true)){
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("first", false);
                        editor.commit();
                        /*Intent intent = new Intent(SplashActivity.this,IntroActivity.class);
                        startActivity(intent);
                        finish();*/
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                }
            }
        };
        timerThread.start();
    }
}
