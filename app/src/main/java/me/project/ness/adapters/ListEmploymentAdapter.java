package me.project.ness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.project.ness.MainActivity;
import me.project.ness.R;
import me.project.ness.objects.News;

/**
 * Created by anjas on 29/10/17.
 */
public class ListEmploymentAdapter extends RecyclerView.Adapter<ListEmploymentViewHolder>{
    private Context context;
    private ArrayList<News> itemList;
    public ArrayList<News> resultData;

    public ListEmploymentAdapter(Context context, ArrayList<News> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
        this.resultData = new ArrayList<>();
        this.resultData.addAll(itemList);
    }

    @Override
    public ListEmploymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null);
        ListEmploymentViewHolder view = new ListEmploymentViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListEmploymentViewHolder holder, int position) {
        holder.title.setText(resultData.get(position).getName());
        holder.date.setText(resultData.get(position).getCreated_at());
        holder.description.setText(resultData.get(position).getDescription());
        Picasso.with(context)
                .load(resultData.get(position).getImage())
                .fit()
                .into(holder.image);


    }

    @Override
    public int getItemCount() {
        return (null != resultData ? resultData.size() : 0);
    }

    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                resultData.clear();
                if (TextUtils.isEmpty(text)) {
                    resultData.addAll(itemList);
                } else {
                    for (News item : itemList) {
                        if (item.getName().toLowerCase().contains(text.toLowerCase())) {
                            resultData.add(item);
                        }
                    }
                }
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });
            }
        }).start();

    }
}