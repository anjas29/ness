package me.project.ness.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import me.project.ness.R;

public class ListEmploymentViewHolder extends RecyclerView.ViewHolder{
    public TextView title;
    public ImageView image;
    public TextView date;
    public TextView description;

    public ListEmploymentViewHolder(final View v) {
        super(v);
        title = (TextView) v.findViewById(R.id.name);
        image = (ImageView) v.findViewById(R.id.avatar);
        date = (TextView) v.findViewById(R.id.date);
        description = (TextView) v.findViewById(R.id.description);


    }
}