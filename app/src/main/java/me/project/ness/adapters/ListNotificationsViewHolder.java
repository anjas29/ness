package me.project.ness.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.project.ness.R;

/**
 * Created by anjas on 29/10/17.
 */

public class ListNotificationsViewHolder extends RecyclerView.ViewHolder{
    public TextView title;
    public ImageView image;

    public CardView cardView;

    public ListNotificationsViewHolder(final View v) {
        super(v);
        title = (TextView) v.findViewById(R.id.title);
        image = (ImageView) v.findViewById(R.id.image);
        cardView = (CardView) v.findViewById(R.id.card_view);
    }
}
