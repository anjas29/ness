package me.project.ness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import me.project.ness.MainActivity;
import me.project.ness.R;
import me.project.ness.objects.News;


/**
 * Created by me on 10/11/16.
 */

public class ListNewsAdapter  extends RecyclerView.Adapter<ListNewsViewHolder>{
    private Context context;
    private ArrayList<News> itemList;

    public ListNewsAdapter(Context context, ArrayList<News> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public ListNewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, null);
        ListNewsViewHolder view = new ListNewsViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListNewsViewHolder holder, int position) {
        holder.title.setText(itemList.get(position).getName());
        Picasso.with(context)
                .load(itemList.get(position).getImage())
                .fit()
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


}