package me.project.ness.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.project.ness.R;

public class ListNewsViewHolder extends RecyclerView.ViewHolder{
    public TextView title;
    public ImageView image;

    public CardView cardView;

    public ListNewsViewHolder(final View v) {
        super(v);
        title = (TextView) v.findViewById(R.id.title);
        image = (ImageView) v.findViewById(R.id.image);
        cardView = (CardView) v.findViewById(R.id.card_view);
    }
}