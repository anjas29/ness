package me.project.ness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.project.ness.R;
import me.project.ness.objects.News;

/**
 * Created by anjas on 29/10/17.
 */
public class ListNotificationsAdapter  extends RecyclerView.Adapter<ListNotificationsViewHolder>{
    private Context context;
    private ArrayList<News> itemList;

    public ListNotificationsAdapter(Context context, ArrayList<News> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public ListNotificationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, null);
        ListNotificationsViewHolder view = new ListNotificationsViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListNotificationsViewHolder holder, int position) {
        holder.title.setText(itemList.get(position).getName());
        Picasso.with(context)
                .load("http://infinitedev.tech/techmeetup.jpeg")
                .fit()
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}